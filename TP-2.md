# TP2 Manipulations de Services



Nommer la machine:



*sudo hostname node1.tp2.linux*


Cependant au redémarrage de la VM le changement ne se sera pas opéré, il faut modifier le fichier hostname:


*sudo nano /etc/hostname*
```markdown
node1.tp2.linux
```
Pour vérifier que le changement a été correctement effectué nous pouvons utiliser ces deux commandes :


*cat /etc/hostname*
```markdown
node1.tp2.linux
```

*hostname*
```markdown
node1.tp2.linux
```

Il faut ensuite configurer notre VM.

Configuration 

VM :

*ip a*

il faut regarder la carte réseau "Réseau privé hôte" qui correspondra ici à enp0s8 (c'est en fait la deuxième) et regarder l'adresse ip qui y est donnée :

```markdown[...] inet 192.168.56.117```

Pour vérifier le bon fonctionnement de la connexion de la VM nous pouvons ping les adresses IP des serveurs DNS de google
 
*ping 8.8.8.8*
```markdown
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=26.7 ms
Il faut faire Ctrl+C pour arrêter le processus
```

*ping ynov.com*
```markdown
PING ynov.com (92.243.16.143) 56(84) bytes of data
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq 1 ttl=50 time=21.3 ms
```

La configuration réseau est bonne, il faut vérifier si notre ordinateur peut ping notre machine.

*ipconfig*
```markdown
[...]Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::a441:453e:7f90:463c%40
  **Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1**
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
   [...]
```
*ping 192.168.56.1*
```markdown
Réponse de 192.168.56.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.56.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
    
Le ping fonctionne parfaitement, nous pouvons donc passer à SSH

Partie 1 : SSH

Installation ssh

Il faut tout d'abord installer le paquet SSH

*sudo apt install openssh-server*
```markdown
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
openssh-server is already the newest version (1:8.4p1-6ubuntu2).
0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.
```

L'installation s'est correctement déroulé, nous pouvons maintenant lancer ssh

Lancement ssh
Il nous faut démarrer ssh :

*systemctl start --now sshd*

Pour vérifier que notre serveur est allumer, nous allons regarder son statut :

*systemctl status sshd*
```markdown
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 10:37:44 CEST; 1h 24min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 528 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 554 (sshd)
      Tasks: 1 (limit: 2314)
     Memory: 3.9M
        CPU: 44ms
     CGroup: /system.slice/ssh.service
             └─554 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
```
Notre service est actuellement actif.

Etude du service ssh

Pour afficher les processus liés au service

*ps ssh*
```markdown
 1000    1385 0000000000000000 0000000000000000 0000000000384004 000000004b813efb Ss+  pts/0      0:00 bash
 1000    1765 0000000000000000 0000000000010000 0000000000384004 000000004b813efb Ss   pts/1      0:00 -bash
 1000    1837 0000000000000000 0000000000000000 0000000000000000 0000000073d1fef9 R+   pts/1      0:00 ps ssh
 ```
 
Pour afficher le port utilisé par le service sshd:

*ss -ltpn*
```markdown
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
```

C'est le port 22 (utilisé de base par SSHD) qui nous intéresse

-Pour afficher les logs du service sshd:

*journalctl -xe -u sshd*
```markdown
[...]-- Journal begins at Tue 2021-10-19 11:33:59 CEST, ends at Mon 2021-10-25 12:17:57 CEST. --
-- No entries --
```

 Ou: 

*journalctl -g sshd*
```markdown
oct. 19 11:52:14 louis-VirtualBox sshd[2797]: pam_unix(sshd:session): session opened for user louis by (uid=0)
oct. 25 11:57:13 node1.tp2.linux sshd[1687]: pam_unix(sshd:session): session opened for user louis by (uid=0)
```

Connexion au serveur:

*ssh louis@192.168.56.117*

Nous sommes maintenant connecté au serveur

Modification de la configuration du serveur
Modifier le comportement du service

Il faut modifier le fichier config de SSH:

*sudo nano /etc/ssh/sshd_config*

Et changer le port 22

*cat /etc/ssh/sshd_config*
```markdown
[...]Port 2021[...]
il faut ensuite redémarrer le service pour que les changements prennent effet
sudo systemctl restart sshd
Et ensuite vérifier si les modifications ont pris effet
ss -latpn
State       Recv-Q       Send-Q              Local Address:Port             Peer Address:Port       Process
[...]
LISTEN      0            128                       0.0.0.0:2021                  0.0.0.0:*
[...]
LISTEN      0            128                          [::]:2021                     [::]:*
```
Se connecter sur le port choisi

*ssh -p2021 192.168.56.117*

Nous voilà connecté au port 2021.

