Installation MariaDB :

```markdown
sudo dnf install -y mariadb-server
```

Savoir sur quel port ecoute MySQL:

```markdown
mathias@macbook-air-de-mathias ~ % sudo ss -p -l -n -u -t | grep mysql
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=1761,fd=21))
```


```markdown
mathias@macbook-air-de-mathias ~ % systemctl status mariadb.service 
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 11:40:06 CET; 4min 25s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 1761 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 11395)
   Memory: 109.6M
   CGroup: /system.slice/mariadb.service
           └─1761 /usr/libexec/mysqld --basedir=/usr

nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: See the MariaDB Knowledgebase at http://mariadb.com/kb or the
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: MySQL manual for more instructions.
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: Please report any problems at http://mariadb.org/jira
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: The latest information about MariaDB is available at http://mariadb.org/.
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: You can find additional information about the MySQL part at:
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: http://dev.mysql.com
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: Consider joining MariaDB's strong and vibrant community:
nov. 25 11:40:05 db.tp5.linux mysql-prepare-db-dir[1656]: https://mariadb.org/get-involved/
nov. 25 11:40:05 db.tp5.linux mysqld[1761]: 2021-11-25 11:40:05 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB) starting as process 1761 ...
nov. 25 11:40:06 db.tp5.linux systemd[1]: Started MariaDB 10.3 database server.
```


Mettre whitlist le port du firewall :

```markdown
sudo firewall-cmd --add-port=3306/tcp --permanent
```

Configuration de MariaDB :

```markdown
mathias@macbook-air-de-mathias ~ % mysql_secure_installation
[...]
Thanks for using MariaDB!
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 17
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```


Création User :

```markdown
mathias@macbook-air-de-mathias ~ % MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```


Connexion à la base de données depuis le serveur web :

```markdown
mathias@macbook-air-de-mathias ~ % mysql -u nextcloud -h 10.5.1.12 -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 21
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```


Installation d'Apache :

```markdown
mathias@macbook-air-de-mathias ~ % sudo dnf install -y httpd
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo systemctl start httpd
```


```markdown
ps aux | grep httpd
root       24342  0.1  0.6 280220 11252 ?        Ss   10:01   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24343  0.0  0.4 294104  8396 ?        S    10:01   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24344  0.0  0.6 1351892 12184 ?       Sl   10:01   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24345  0.0  0.6 1351892 12184 ?       Sl   10:01   0:00 /usr/sbin/httpd -DFOREGROUND
apache     24346  0.0  0.7 1483020 14232 ?       Sl   10:01   0:00 /usr/sbin/httpd -DFOREGROUND
```


Ajout du port de Apache au firewall :

```markdown
mathias@macbook-air-de-mathias ~ % sudo firewall-cmd --add-port=80/tcp --permanent 
success
```

```markdown
mathias@macbook-air-de-mathias ~ %  sudo firewall-cmd --reload
success
```

```markdown
mathias@macbook-air-de-mathias ~ %  sudo nmcli con reload
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo nmcli con up enp0s8
Connexion activée (chemin D-Bus actif : /org/freedesktop/NetworkManager/ActiveConnection/3)
```

```markdown
mathias@macbook-air-de-mathias ~ %  sudo firewall-cmd --list-all
sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols: 
  forward: no
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```


```markdown
mathias@macbook-air-de-mathias ~ %
❯ curl 10.5.1.11
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      
      html {
        height: 100%;
        width: 100%;
      }  
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1); 
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;      
        
      }

   
  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    font-size: 2em;
  }
  h1 strong {
    font-weight: bolder;
    font-family: 'Montserrat', sans-serif;
  }
  h2 {
    font-size: 1.5em;
    font-weight:bold;
  }
  
  .title {
    border: 1px solid black;
    font-weight: bold;
    position: relative;
    float: right;
    width: 150px;
    text-align: center;
    padding: 10px 0 10px 0;
    margin-top: 0;
  }
  
  .description {
    padding: 45px 10px 5px 10px;
    clear: right;
    padding: 15px;
  }
  
  .section {
    padding-left: 3%;
   margin-bottom: 10px;
  }
  
  img {
  
    padding: 2px;
    margin: 2px;
  }
  a:hover img {
    padding: 2px;
    margin: 2px;
  }

  :link {
    color: rgb(199, 252, 77);
    text-shadow:
  }
  :visited {
    color: rgb(122, 206, 255);
  }
  a:hover {
    color: rgb(16, 44, 122);
  }
  .row {
    width: 100%;
    padding: 0 10px 0 10px;
  }
  
  footer {
    padding-top: 6em;
    margin-bottom: 6em;
    text-align: center;
    font-size: xx-small;
    overflow:hidden;
    clear: both;
  }
 
  .summary {
    font-size: 140%;
    text-align: center;
  }

  #rocky-poweredby img {
    margin-left: -10px;
  }

  #logos img {
    vertical-align: top;
  }
  
  /* Desktop  View Options */
 
  @media (min-width: 768px)  {
  
    body {
      padding: 10em 20% !important;
    }
    
    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6,
    .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }
  
    .col-md-1 {
      width: 8.33%;
    }
    .col-md-2 {
      width: 16.66%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-4 {
      width: 33%;
    }
    .col-md-5 {
      width: 41.66%;
    }
    .col-md-6 {
      border-left:3px ;
      width: 50%;
      

    }
    .col-md-7 {
      width: 58.33%;
    }
    .col-md-8 {
      width: 66.66%;
    }
    .col-md-9 {
      width: 74.99%;
    }
    .col-md-10 {
      width: 83.33%;
    }
    .col-md-11 {
      width: 91.66%;
    }
    .col-md-12 {
      width: 100%;
    }
  }
  
  /* Mobile View Options */
  @media (max-width: 767px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }
  
    .col-sm-1 {
      width: 8.33%;
    }
    .col-sm-2 {
      width: 16.66%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-4 {
      width: 33%;
    }
    .col-sm-5 {
      width: 41.66%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-7 {
      width: 58.33%;
    }
    .col-sm-8 {
      width: 66.66%;
    }
    .col-sm-9 {
      width: 74.99%;
    }
    .col-sm-10 {
      width: 83.33%;
    }
    .col-sm-11 {
      width: 91.66%;
    }
    .col-sm-12 {
      width: 100%;
    }
    h1 {
      padding: 0 !important;
    }
  }
        
  
  </style>
  </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>
    
      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>
      
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
     
       
        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>
    
          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>
         
          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>
        
        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src= "icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>       
      </div>
      </div>
      
      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>
      
  </body>
</html>
```


Configuration d'Apache :

```markdown
mathias@macbook-air-de-mathias ~ % grep conf.d /etc/httpd/conf/httpd.conf
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.con
```


```markdown
mathias@macbook-air-de-mathias ~ % cat nextcloud.conf 
<VirtualHost *:80>
  DocumentRoot /var/www/nextcloud/html/  
  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo mkdir -p /var/www/nextcloud/html/
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo chown -R apache:apache /var/www
```

```markdown
mathias@macbook-air-de-mathias ~ % cat /etc/opt/remi/php74/php.ini | grep date.timezone
; http://php.net/date.timezone
date.timezone = "Europe/Paris"
```


```markdown
mathias@macbook-air-de-mathias ~ % curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0   577k      0  0:04:22  0:04:22 --:--:--  448k
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo mv nextcloud/* /var/www/nextcloud/html
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo chown -R apache:apache /var/www
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo systemctl restart httpd
```


```markdown
mathias@macbook-air-de-mathias ~ %
❯ cat /etc/hosts
127.0.0.1	localhost
127.0.1.1	Linux-Mathias

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
10.5.1.11 web.tp5.linux
```

C'est tout bon !