Choisirune IP et la Définir :

```markdown
mathias@macbook-air-de-mathias ~ % ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=1203<RXCSUM,TXCSUM,TXSTATUS,SW_TIMESTAMP>
	inet 127.0.0.1 netmask 0xff000000 
	inet6 ::1 prefixlen 128 
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1 
	nd6 options=201<PERFORMNUD,DAD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
anpi1: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 7e:e1:4b:ba:04:42 
	inet6 fe80::7ce1:4bff:feba:442%anpi1 prefixlen 64 scopeid 0x4 
	nd6 options=201<PERFORMNUD,DAD>
	media: none
	status: inactive
anpi0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 7e:e1:4b:ba:04:41 
	inet6 fe80::7ce1:4bff:feba:441%anpi0 prefixlen 64 scopeid 0x5 
	nd6 options=201<PERFORMNUD,DAD>
	media: none
	status: inactive
en3: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 7e:e1:4b:ba:04:21 
	nd6 options=201<PERFORMNUD,DAD>
	media: none
	status: inactive
en4: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 7e:e1:4b:ba:04:22 
	nd6 options=201<PERFORMNUD,DAD>
	media: none
	status: inactive
en1: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=460<TSO4,TSO6,CHANNEL_IO>
	ether 36:98:df:60:c9:40 
	media: autoselect <full-duplex>
	status: inactive
en2: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=460<TSO4,TSO6,CHANNEL_IO>
	ether 36:98:df:60:c9:44 
	media: autoselect <full-duplex>
	status: inactive

```

l'ip de l'interface est 192.168.1.11
ma machine comporte l'ip 192.168.1.69

Connexion SSH fonctionelle :

```markdown
mathias@macbook-air-de-mathias ~ % ssh mathias@192.168.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Nov 23 13:19:48 2021 from 192.168.1.11
```

Connexion internet :

```markdown
mathias@macbook-air-de-mathias ~ % ping google.com
PING google.com (216.58.215.46): 56 data bytes
64 bytes from 216.58.215.46: icmp_seq=0 ttl=115 time=18.869 ms
64 bytes from 216.58.215.46: icmp_seq=1 ttl=115 time=29.735 ms
64 bytes from 216.58.215.46: icmp_seq=2 ttl=115 time=27.788 ms
64 bytes from 216.58.215.46: icmp_seq=3 ttl=115 time=19.151 ms
64 bytes from 216.58.215.46: icmp_seq=4 ttl=115 time=26.945 ms
```

Test résolution du nom :

```markdown
mathias@macbook-air-de-mathias ~ % ping google.com -c 4
PING google.com (216.58.215.46): 56 data bytes
64 bytes from 216.58.215.46: icmp_seq=0 ttl=115 time=27.669 ms
64 bytes from 216.58.215.46: icmp_seq=1 ttl=115 time=27.354 ms
64 bytes from 216.58.215.46: icmp_seq=2 ttl=115 time=27.782 ms
64 bytes from 216.58.215.46: icmp_seq=3 ttl=115 time=27.541 ms

--- google.com ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 27.354/27.586/27.782/0.159 ms
```

Donner un nom a notre machine :

```markdown
mathias@macbook-air-de-mathias ~ % cat /etc/hostname
node1.tp_4.linux

mathias@macbook-air-de-mathias ~ % hostname
node1.tp_4.linux
```

Vérification si nginx bien installer :

```markdown
mathias@macbook-air-de-mathias ~ % sudo systemctl status nginx
 nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-23 13:31:05 CET; 1s ago
  Process: 1661 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 1659 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 1658 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 1663 (nginx)
    Tasks: 2 (limit: 11395)
   Memory: 12.1M
   CGroup: /system.slice/nginx.service
           ├─1663 nginx: master process /usr/sbin/nginx
           └─1664 nginx: worker process

nov. 23 13:31:05 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
nov. 23 13:31:05 node1.tp4.linux nginx[1659]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nov. 23 13:31:05 node1.tp4.linux nginx[1659]: nginx: configuration file /etc/nginx/nginx.conf test is successful
nov. 23 13:31:05 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argum>
nov. 23 13:31:05 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```

Configurer le firewall :
```markdown
mathias@macbook-air-de-mathias ~ % sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```

Connextion au site web :
```markdown
curl 192.168.1.11:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtPilll/DTD/xhtml11.dtd
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<style type="text/css">
/*<![CDATA[*/
body {
background-color: #fff;
color: #000;
font-size: 0.9em;
font-fapiily: sans-serif, helvettica;
margin: O;
padding: 0;
}
```

Modification de la configuration du serveur web :

```markdown
mathias@macbook-air-de-mathias ~ % sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8889/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

résultat : 

```markdown
mathias@macbook-air-de-mathias ~ % sudo ss -l -p -n -u -t | grep nginx
tcp   LISTEN 0      128          0.0.0.0:8889      0.0.0.0:*    users:(("nginx",pid=2180,fd=9),("nginx",pid=2179,fd=9))
tcp   LISTEN 0      128             [::]:8889        [::]:*    users:(("nginx",pid=2180,fd=10),("nginx",pid=2179,fd=10))
```