Préparation de la machine : 



Ajout d'un disque dur de 5 Go à la VM :

```markdown
mathias@macbook-air-de-mathias ~ % lsblk | grep sdb
sdb           8:16   0    5G  0 disk
```


Partitionner le disque :
```markdown
mathias@macbook-air-de-mathias ~ % sudo pvcreate /dev/sdb
[sudo] password for Mathias:
  Physical volume "/dev/sdb" successfully created.
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   5.00g 5.00g
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo pvdisplay
 [...]
  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               dgX71v-rPQU-TMuE-eCpx-d9d3-EGi9-kqs32k
```


Créer un volume group : 

```markdown
mathias@macbook-air-de-mathias ~ % sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
```

Créer la partition utilisable :

```markdown
mathias@macbook-air-de-mathias ~ % sudo lvcreate -l 100%FREE backup -n stockage
  Logical volume "stockage" created.
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo lvs
  LV       VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  stockage backup -wi-a-----  <5.00g
  root     rl     -wi-ao----  <6.20g
  swap     rl     -wi-ao---- 820.00m
```
```markdown
mathias@macbook-air-de-mathias ~ %sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/stockage
  LV Name                stockage
  VG Name                backup
  LV UUID                vlLDUr-vH1x-fu2B-sZEy-d9nC-PhBJ-SGQB4R
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-11-30 10:58:34 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
[...]
```

Formater la partition :

```markdown
mathias@macbook-air-de-mathias ~ % sudo !!
sudo mkfs -t ext4 /dev/backup/stockage
[sudo] password for Mathias:
mke2fs 1.45.6 (22-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: 4eb39983-be87-4cb6-bc98-3b12123d6fff
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```

Partie 2 : Setup du serveur NFS :


Préparer les dossiers à partager :

```markdown
mathias@macbook-air-de-mathias ~ % sudo mkdir backup
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo mkdir /backup/web.tp6.linux
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo mkdir /backup/db.tp6.linux
```
```markdown
mathias@macbook-air-de-mathias ~ % ls /backup/
```

Installation du serveur NFS :

```markdown
mathias@macbook-air-de-mathias ~ % sudo dnf install nfs-utils
[...]

Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```

Configuration du serveur :

```markdown
mathias@macbook-air-de-mathias ~ % cat /etc/idmapd.conf | grep Domain | head -n 1
Domain = tp6.linux
mathias@macbook-air-de-mathias ~ % cat /etc/exports
/backup/web.tp6.linux 10.5.1.0/24(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.0/24(rw,no_root_squash)
```

Démmarer le serveur :

```markdown
mathias@macbook-air-de-mathias ~ % sudo systemctl status nfs-server.service
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-11-30 12:46:44 CET; 8s ago
  Process: 24532 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=>
  Process: 24520 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 24519 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
 Main PID: 24532 (code=exited, status=0/SUCCESS)

Dec 7 12:46:44 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 7 12:46:44 backup.tp6.linux systemd[1]: Started NFS server and services.
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo systemctl enable nfs-server.service
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

Mise en place du Firewall :

```markdown
mathias@macbook-air-de-mathias ~ % sudo firewall-cmd --add-port=2049/tcp --permanent
success
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo firewall-cmd --reload
success
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo ss -ltpn | grep 2049
LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*
LISTEN 0      64              [::]:2049          [::]:*
```


Partie 3 : Installation des clients NFS :

```markdown
mathias@macbook-air-de-mathias ~ % sudo dnf install nfs-utils
[sudo] password for Mathias:
[...]
Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```

Montage :

```markdown
mathias@macbook-air-de-mathias ~ % sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux/ /srv/backup
```
```markdown
mathias@macbook-air-de-mathias ~ % df -h | tail -n 1
10.5.1.13:/backup/web.tp6.linux  9.8G   23M  9.3G   1% /srv/backup
```
```markdown
mathias@macbook-air-de-mathias ~ % sudo chown Mathias /srv/backup/
[sudo] password for Mathias:
```
```markdown
mathias@macbook-air-de-mathias ~ % ls -l /srv/
total 4
drwxr-xr-x. 2 Mathias root 4096 Dec 07 12:36 backup
```
```markdown
mathias@macbook-air-de-mathias ~ % tail -n 1 /etc/fstab
10.5.1.13:/backup/web.tp6.linux/ /srv/backup nfs defaults 0 0
```

```markdown
mathias@macbook-air-de-mathias ~ % sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount.nfs: timeout set for Tue Dec 07 13:14:54 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.5.1.13,clientaddr=10.5.1.11'
/srv/backup              : successfully mounted
```